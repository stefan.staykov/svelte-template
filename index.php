<!DOCTYPE html>

<html lang="en">

	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="viewport-fit=cover, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<meta name="format-detection" content="telephone=no"/>
		<meta name="msapplication-tap-highlight" content="no"/>

		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<meta name="apple-mobile-web-app-title" content="Ionic PWA"/>
		<meta name="apple-mobile-web-app-status-bar-style" content="black"/>

		<meta http-equiv="x-ua-compatible" content="IE=Edge"/>
        <?php wp_head() ?>
	</head>
	
	<body>
		<?php wp_footer() ?>
	</body>

</html>